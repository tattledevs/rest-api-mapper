<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 11:05 AM
 * Project: rest-api-mapper
 */
namespace RestApiMapper;

/**
 * Class AdvancedSetterTrait
 * @package RestApiMapper
 */
trait AdvancedSetterTrait {


    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if( stristr($name, 'date_time')
            || stristr($name, 'date_')
            || stristr($name, '_date')
            || $name == 'birthdate')
        {
            if ( ! isset($this->processDates) || $this->processDates )
            {
                $dt = new \DateTime($value);
                $value = $dt->getTimestamp() * 1000;
            }
        }

        $this->$name = $value;
    }
} 